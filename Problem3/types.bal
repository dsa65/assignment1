import ballerina/http;

public type CreatedInlineResponse201 record {|
    *http:Created;
    InlineResponse201 body;
|};

public type InlineResponse201 record {
    # the studentnumber of the student newly created
    string studentnumber?;
};

public type Error record {
    string errorType?;
    string message?;
};

public type Student record {
    # the studentnumber of the student
    string studentnumber?;
    # the studentname of the student
    string studentname?;
    # the email address of the student
    string email?;
    # the email address of the student
    string course?;
};
