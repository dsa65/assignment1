import ballerina/http;
import  ballerina/io;

Student[] allstudent =[];

listener http:Listener ep0 = new (8080, config = {host: "localhost"});

service /students/api/v1 on ep0 {
    resource function get students() returns Student[]|http:Response {
        io:print("Student registered...\n");
        return allstudent;
    }
    resource function post students(@http:Payload Student addedstudent) returns json |http:Response {
        io:print("Adding one user....");
        allstudent.push(addedstudent);
        return  {"":"You have successfully added"};
    }
    resource function get users/[string  studentnumber]() returns Student|http:Response {
        Student wordpassed = {};
      foreach var item in allstudent {
        if(item.studentnumber==studentnumber){
        wordpassed=item;
        }
      }
     return wordpassed;
    }
    resource function put users/[string  studentnumber](@http:Payload Student payload) returns Student|http:Response {
        http:Response r =new;
      return r;
    }
    resource function delete users/[string  studentnumber]() returns http:NoContent|http:Response {
        http:Response r =new;
      return r;
    }
}
