import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR_HELO, descMap: getDescriptorMapHelo()}
service "problem1" on ep {

    remote function assignCourse(course value) returns course_response|error {
    }
    remote function submitMarks(submit_marks value) returns submit_marks_response|error {
    }
    remote function viewMarks(view_marks value) returns view_marks_response|error {
    }
    remote function createUser(stream<create_user, grpc:Error?> clientStream) returns create_user_response|error {
    }
    remote function submitAssignment(stream<submit_assignment, grpc:Error?> clientStream) returns submit_assignment_response|error {
    }
    remote function register(stream<registration, grpc:Error?> clientStream) returns registration_response|error {
    }
    remote function requestAssignment(get_assignment value) returns stream<get_assignment_response, error?>|error {
    }
    remote function createCourse(stream<create_course, grpc:Error?> clientStream) returns stream<create_course_response, error?>|error {
    }
}

