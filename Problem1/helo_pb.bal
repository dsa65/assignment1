import ballerina/grpc;

public isolated client class problem1Client {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR_HELO, getDescriptorMapHelo());
    }

    isolated remote function assignCourse(course|ContextCourse req) returns course_response|grpc:Error {
        map<string|string[]> headers = {};
        course message;
        if req is ContextCourse {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("problem1/assignCourse", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <course_response>result;
    }

    isolated remote function assignCourseContext(course|ContextCourse req) returns ContextCourse_response|grpc:Error {
        map<string|string[]> headers = {};
        course message;
        if req is ContextCourse {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("problem1/assignCourse", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <course_response>result, headers: respHeaders};
    }

    isolated remote function submitMarks(submit_marks|ContextSubmit_marks req) returns submit_marks_response|grpc:Error {
        map<string|string[]> headers = {};
        submit_marks message;
        if req is ContextSubmit_marks {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("problem1/submitMarks", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <submit_marks_response>result;
    }

    isolated remote function submitMarksContext(submit_marks|ContextSubmit_marks req) returns ContextSubmit_marks_response|grpc:Error {
        map<string|string[]> headers = {};
        submit_marks message;
        if req is ContextSubmit_marks {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("problem1/submitMarks", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <submit_marks_response>result, headers: respHeaders};
    }

    isolated remote function viewMarks(view_marks|ContextView_marks req) returns view_marks_response|grpc:Error {
        map<string|string[]> headers = {};
        view_marks message;
        if req is ContextView_marks {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("problem1/viewMarks", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <view_marks_response>result;
    }

    isolated remote function viewMarksContext(view_marks|ContextView_marks req) returns ContextView_marks_response|grpc:Error {
        map<string|string[]> headers = {};
        view_marks message;
        if req is ContextView_marks {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("problem1/viewMarks", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <view_marks_response>result, headers: respHeaders};
    }

    isolated remote function createUser() returns CreateUserStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("problem1/createUser");
        return new CreateUserStreamingClient(sClient);
    }

    isolated remote function submitAssignment() returns SubmitAssignmentStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("problem1/submitAssignment");
        return new SubmitAssignmentStreamingClient(sClient);
    }

    isolated remote function register() returns RegisterStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("problem1/register");
        return new RegisterStreamingClient(sClient);
    }

    isolated remote function requestAssignment(get_assignment|ContextGet_assignment req) returns stream<get_assignment_response, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        get_assignment message;
        if req is ContextGet_assignment {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("problem1/requestAssignment", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        Get_assignment_responseStream outputStream = new Get_assignment_responseStream(result);
        return new stream<get_assignment_response, grpc:Error?>(outputStream);
    }

    isolated remote function requestAssignmentContext(get_assignment|ContextGet_assignment req) returns ContextGet_assignment_responseStream|grpc:Error {
        map<string|string[]> headers = {};
        get_assignment message;
        if req is ContextGet_assignment {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("problem1/requestAssignment", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        Get_assignment_responseStream outputStream = new Get_assignment_responseStream(result);
        return {content: new stream<get_assignment_response, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function createCourse() returns CreateCourseStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("problem1/createCourse");
        return new CreateCourseStreamingClient(sClient);
    }
}

public client class CreateUserStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCreate_user(create_user message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCreate_user(ContextCreate_user message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveCreate_user_response() returns create_user_response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <create_user_response>payload;
        }
    }

    isolated remote function receiveContextCreate_user_response() returns ContextCreate_user_response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <create_user_response>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class SubmitAssignmentStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendSubmit_assignment(submit_assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextSubmit_assignment(ContextSubmit_assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveSubmit_assignment_response() returns submit_assignment_response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <submit_assignment_response>payload;
        }
    }

    isolated remote function receiveContextSubmit_assignment_response() returns ContextSubmit_assignment_response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <submit_assignment_response>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class RegisterStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendRegistration(registration message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextRegistration(ContextRegistration message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveRegistration_response() returns registration_response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <registration_response>payload;
        }
    }

    isolated remote function receiveContextRegistration_response() returns ContextRegistration_response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <registration_response>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class Get_assignment_responseStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|get_assignment_response value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|get_assignment_response value;|} nextRecord = {value: <get_assignment_response>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class CreateCourseStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCreate_course(create_course message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCreate_course(ContextCreate_course message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveCreate_course_response() returns create_course_response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <create_course_response>payload;
        }
    }

    isolated remote function receiveContextCreate_course_response() returns ContextCreate_course_response|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <create_course_response>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Problem1SubmitassignmentresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendSubmit_assignment_response(submit_assignment_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextSubmit_assignment_response(ContextSubmit_assignment_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class Problem1ViewmarksresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendView_marks_response(view_marks_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextView_marks_response(ContextView_marks_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class Problem1CreatecourseresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCreate_course_response(create_course_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCreate_course_response(ContextCreate_course_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class Problem1GetassignmentresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendGet_assignment_response(get_assignment_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextGet_assignment_response(ContextGet_assignment_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class Problem1CreateuserresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCreate_user_response(create_user_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCreate_user_response(ContextCreate_user_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class Problem1SubmitmarksresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendSubmit_marks_response(submit_marks_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextSubmit_marks_response(ContextSubmit_marks_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class Problem1CourseresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCourse_response(course_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCourse_response(ContextCourse_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class Problem1RegistrationresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendRegistration_response(registration_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextRegistration_response(ContextRegistration_response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextGet_assignment_responseStream record {|
    stream<get_assignment_response, error?> content;
    map<string|string[]> headers;
|};

public type ContextCreate_course_responseStream record {|
    stream<create_course_response, error?> content;
    map<string|string[]> headers;
|};

public type ContextSubmit_assignmentStream record {|
    stream<submit_assignment, error?> content;
    map<string|string[]> headers;
|};

public type ContextRegistrationStream record {|
    stream<registration, error?> content;
    map<string|string[]> headers;
|};

public type ContextCreate_userStream record {|
    stream<create_user, error?> content;
    map<string|string[]> headers;
|};

public type ContextCreate_courseStream record {|
    stream<create_course, error?> content;
    map<string|string[]> headers;
|};

public type ContextCreate_user_response record {|
    create_user_response content;
    map<string|string[]> headers;
|};

public type ContextGet_assignment record {|
    get_assignment content;
    map<string|string[]> headers;
|};

public type ContextGet_assignment_response record {|
    get_assignment_response content;
    map<string|string[]> headers;
|};

public type ContextView_marks_response record {|
    view_marks_response content;
    map<string|string[]> headers;
|};

public type ContextCourse_response record {|
    course_response content;
    map<string|string[]> headers;
|};

public type ContextCreate_course_response record {|
    create_course_response content;
    map<string|string[]> headers;
|};

public type ContextSubmit_marks record {|
    submit_marks content;
    map<string|string[]> headers;
|};

public type ContextSubmit_assignment_response record {|
    submit_assignment_response content;
    map<string|string[]> headers;
|};

public type ContextSubmit_marks_response record {|
    submit_marks_response content;
    map<string|string[]> headers;
|};

public type ContextRegistration_response record {|
    registration_response content;
    map<string|string[]> headers;
|};

public type ContextCourse record {|
    course content;
    map<string|string[]> headers;
|};

public type ContextSubmit_assignment record {|
    submit_assignment content;
    map<string|string[]> headers;
|};

public type ContextRegistration record {|
    registration content;
    map<string|string[]> headers;
|};

public type ContextCreate_user record {|
    create_user content;
    map<string|string[]> headers;
|};

public type ContextView_marks record {|
    view_marks content;
    map<string|string[]> headers;
|};

public type ContextCreate_course record {|
    create_course content;
    map<string|string[]> headers;
|};

public type create_user_response record {|
    string message = "";
|};

public type get_assignment record {|
    string assdescription = "";
|};

public type get_assignment_response record {|
    string assdescription = "";
    string coursecode = "";
    string asscontent = "";
    string studentId = "";
|};

public type view_marks_response record {|
    string studentId = "";
    string assdescription = "";
    int marks = 0;
    int weight = 0;
|};

public type course_response record {|
    string message = "";
|};

public type create_course_response record {|
    create_course code = {};
|};

public type submit_marks record {|
    string studentId = "";
    string assdescription = "";
    int marks = 0;
    int weght = 0;
|};

public type submit_assignment_response record {|
    string message = "";
|};

public type submit_marks_response record {|
    string message = "";
|};

public type registration_response record {|
    string message = "";
|};

public type course record {|
    string code = "";
    string assessorId = "";
|};

public type submit_assignment record {|
    string studentId = "";
    string coursecode = "";
    string assdescription = "";
    string asscontent = "";
|};

public type registration record {|
    string coursecode = "";
    string studentId = "";
|};

public type create_user record {|
    string username = "";
    string firstname = "";
    string lastname = "";
    string profile = "";
|};

public type view_marks record {|
    string studentId = "";
|};

public type create_course record {|
    string code = "";
    int weight = 0;
    int numberofassessment = 0;
|};

const string ROOT_DESCRIPTOR_HELO = "0A0A68656C6F2E70726F746F226B0A0D6372656174655F636F7572736512120A04636F64651801200128095204636F646512160A067765696768741802200128055206776569676874122E0A126E756D6265726F666173736573736D656E7418032001280552126E756D6265726F666173736573736D656E74223C0A166372656174655F636F757273655F726573706F6E736512220A04636F646518012001280B320E2E6372656174655F636F757273655204636F6465227D0A0B6372656174655F75736572121A0A08757365726E616D651801200128095208757365726E616D65121C0A0966697273746E616D65180220012809520966697273746E616D65121A0A086C6173746E616D6518032001280952086C6173746E616D6512180A0770726F66696C65180420012809520770726F66696C6522300A146372656174655F757365725F726573706F6E736512180A076D65737361676518012001280952076D657373616765223C0A06636F7572736512120A04636F64651801200128095204636F6465121E0A0A6173736573736F724964180220012809520A6173736573736F724964222B0A0F636F757273655F726573706F6E736512180A076D65737361676518012001280952076D6573736167652299010A117375626D69745F61737369676E6D656E74121C0A0973747564656E744964180120012809520973747564656E744964121E0A0A636F75727365636F6465180220012809520A636F75727365636F646512260A0E6173736465736372697074696F6E180320012809520E6173736465736372697074696F6E121E0A0A617373636F6E74656E74180420012809520A617373636F6E74656E7422360A1A7375626D69745F61737369676E6D656E745F726573706F6E736512180A076D65737361676518012001280952076D65737361676522380A0E6765745F61737369676E6D656E7412260A0E6173736465736372697074696F6E180120012809520E6173736465736372697074696F6E229F010A176765745F61737369676E6D656E745F726573706F6E736512260A0E6173736465736372697074696F6E180120012809520E6173736465736372697074696F6E121E0A0A636F75727365636F6465180220012809520A636F75727365636F6465121E0A0A617373636F6E74656E74180320012809520A617373636F6E74656E74121C0A0973747564656E744964180420012809520973747564656E7449642280010A0C7375626D69745F6D61726B73121C0A0973747564656E744964180120012809520973747564656E74496412260A0E6173736465736372697074696F6E180220012809520E6173736465736372697074696F6E12140A056D61726B7318032001280552056D61726B7312140A0577656768741804200128055205776567687422310A157375626D69745F6D61726B735F726573706F6E736512180A076D65737361676518012001280952076D657373616765224C0A0C726567697374726174696F6E121E0A0A636F75727365636F6465180120012809520A636F75727365636F6465121C0A0973747564656E744964180220012809520973747564656E74496422310A15726567697374726174696F6E5F726573706F6E736512180A076D65737361676518012001280952076D657373616765222A0A0A766965775F6D61726B73121C0A0973747564656E744964180120012809520973747564656E7449642289010A13766965775F6D61726B735F726573706F6E7365121C0A0973747564656E744964180120012809520973747564656E74496412260A0E6173736465736372697074696F6E180220012809520E6173736465736372697074696F6E12140A056D61726B7318032001280552056D61726B7312160A06776569676874180420012805520677656967687432CB030A0870726F626C656D31123B0A0C637265617465436F75727365120E2E6372656174655F636F757273651A172E6372656174655F636F757273655F726573706F6E73652801300112330A0A63726561746555736572120C2E6372656174655F757365721A152E6372656174655F757365725F726573706F6E7365280112290A0C61737369676E436F7572736512072E636F757273651A102E636F757273655F726573706F6E736512450A107375626D697441737369676E6D656E7412122E7375626D69745F61737369676E6D656E741A1B2E7375626D69745F61737369676E6D656E745F726573706F6E7365280112400A117265717565737441737369676E6D656E74120F2E6765745F61737369676E6D656E741A182E6765745F61737369676E6D656E745F726573706F6E7365300112340A0B7375626D69744D61726B73120D2E7375626D69745F6D61726B731A162E7375626D69745F6D61726B735F726573706F6E736512330A087265676973746572120D2E726567697374726174696F6E1A162E726567697374726174696F6E5F726573706F6E73652801122E0A09766965774D61726B73120B2E766965775F6D61726B731A142E766965775F6D61726B735F726573706F6E7365620670726F746F33";

public isolated function getDescriptorMapHelo() returns map<string> {
    return {"helo.proto": "0A0A68656C6F2E70726F746F226B0A0D6372656174655F636F7572736512120A04636F64651801200128095204636F646512160A067765696768741802200128055206776569676874122E0A126E756D6265726F666173736573736D656E7418032001280552126E756D6265726F666173736573736D656E74223C0A166372656174655F636F757273655F726573706F6E736512220A04636F646518012001280B320E2E6372656174655F636F757273655204636F6465227D0A0B6372656174655F75736572121A0A08757365726E616D651801200128095208757365726E616D65121C0A0966697273746E616D65180220012809520966697273746E616D65121A0A086C6173746E616D6518032001280952086C6173746E616D6512180A0770726F66696C65180420012809520770726F66696C6522300A146372656174655F757365725F726573706F6E736512180A076D65737361676518012001280952076D657373616765223C0A06636F7572736512120A04636F64651801200128095204636F6465121E0A0A6173736573736F724964180220012809520A6173736573736F724964222B0A0F636F757273655F726573706F6E736512180A076D65737361676518012001280952076D6573736167652299010A117375626D69745F61737369676E6D656E74121C0A0973747564656E744964180120012809520973747564656E744964121E0A0A636F75727365636F6465180220012809520A636F75727365636F646512260A0E6173736465736372697074696F6E180320012809520E6173736465736372697074696F6E121E0A0A617373636F6E74656E74180420012809520A617373636F6E74656E7422360A1A7375626D69745F61737369676E6D656E745F726573706F6E736512180A076D65737361676518012001280952076D65737361676522380A0E6765745F61737369676E6D656E7412260A0E6173736465736372697074696F6E180120012809520E6173736465736372697074696F6E229F010A176765745F61737369676E6D656E745F726573706F6E736512260A0E6173736465736372697074696F6E180120012809520E6173736465736372697074696F6E121E0A0A636F75727365636F6465180220012809520A636F75727365636F6465121E0A0A617373636F6E74656E74180320012809520A617373636F6E74656E74121C0A0973747564656E744964180420012809520973747564656E7449642280010A0C7375626D69745F6D61726B73121C0A0973747564656E744964180120012809520973747564656E74496412260A0E6173736465736372697074696F6E180220012809520E6173736465736372697074696F6E12140A056D61726B7318032001280552056D61726B7312140A0577656768741804200128055205776567687422310A157375626D69745F6D61726B735F726573706F6E736512180A076D65737361676518012001280952076D657373616765224C0A0C726567697374726174696F6E121E0A0A636F75727365636F6465180120012809520A636F75727365636F6465121C0A0973747564656E744964180220012809520973747564656E74496422310A15726567697374726174696F6E5F726573706F6E736512180A076D65737361676518012001280952076D657373616765222A0A0A766965775F6D61726B73121C0A0973747564656E744964180120012809520973747564656E7449642289010A13766965775F6D61726B735F726573706F6E7365121C0A0973747564656E744964180120012809520973747564656E74496412260A0E6173736465736372697074696F6E180220012809520E6173736465736372697074696F6E12140A056D61726B7318032001280552056D61726B7312160A06776569676874180420012805520677656967687432CB030A0870726F626C656D31123B0A0C637265617465436F75727365120E2E6372656174655F636F757273651A172E6372656174655F636F757273655F726573706F6E73652801300112330A0A63726561746555736572120C2E6372656174655F757365721A152E6372656174655F757365725F726573706F6E7365280112290A0C61737369676E436F7572736512072E636F757273651A102E636F757273655F726573706F6E736512450A107375626D697441737369676E6D656E7412122E7375626D69745F61737369676E6D656E741A1B2E7375626D69745F61737369676E6D656E745F726573706F6E7365280112400A117265717565737441737369676E6D656E74120F2E6765745F61737369676E6D656E741A182E6765745F61737369676E6D656E745F726573706F6E7365300112340A0B7375626D69744D61726B73120D2E7375626D69745F6D61726B731A162E7375626D69745F6D61726B735F726573706F6E736512330A087265676973746572120D2E726567697374726174696F6E1A162E726567697374726174696F6E5F726573706F6E73652801122E0A09766965774D61726B73120B2E766965775F6D61726B731A142E766965775F6D61726B735F726573706F6E7365620670726F746F33"};
}

