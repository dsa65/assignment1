import ballerina/http;
import ballerina/graphql;


service /graphql on new graphql:Listener(4000) {



private Statistics stats;

 function init() {
    
        self.stats = new("12/09/2021", "khomas", 30, 400,60,1000);
    }

    resource function get getStatistics() returns Statistics {
        return self.stats;
    }

     remote function updateDate(string date) returns Statistics{
        self.stats.setDate(date);
         return self.stats;
     }

      remote function updateRegion(string region) returns Statistics{
        self.stats.setRegion(region);
         return self.stats;
     }

      remote function updateDeaths(int deaths) returns Statistics{
        self.stats.setDeaths(deaths);
         return self.stats;
     }

     remote function updateConfirmed(int confirmed_cases) returns Statistics{
        self.stats.setConfirmed(confirmed_cases);
         return self.stats;
     }

      remote function updateRecoveries(int recoveries) returns Statistics{
        self.stats.setRecoveries(recoveries);
         return self.stats;
     }

     remote function updateTested(int tested) returns Statistics{
        self.stats.setTested(tested);
         return self.stats;
     }

    
}



public service class Statistics {
 
   private string date;
   private string region;
   private  int deaths;
   private  int confirmed_cases;
   private  int recoveries;
   private  int tested;


function init(string date, string region, int deaths, int confirmed_cases, int recoveries, int tested) {
  
        self.date = date;
        self.region = region;
        self.deaths = deaths;
        self.confirmed_cases=confirmed_cases;
        self.recoveries=recoveries;
        self.tested=tested;


}

resource function get date() returns string {
        return self.date;
    }

resource function get region() returns string {
        return self.region;
    }

resource function get deaths() returns int {
        return self.deaths;
 }

resource function get confirmed_cases() returns int {
        return self.confirmed_cases;
}

resource function get recoveries() returns int {
        return self.recoveries;
}

resource function get tested() returns int {
        return self.tested;
}



function setDate(string date) {
         self.date =date;
    }

function setRegion(string region)  {
        self.region = region;
    }

function setDeaths(int deaths) {
        self.deaths = deaths;
 }

function  setConfirmed(int confirmed_cases) {
        self.confirmed_cases = confirmed_cases ;
}

function setRecoveries(int recoveries) {
           self.recoveries = recoveries;
}

function setTested(int tested) {
        self.tested = tested;
}



